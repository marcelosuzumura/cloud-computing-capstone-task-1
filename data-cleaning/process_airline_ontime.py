import glob
import os
import csv
import zipfile
import StringIO

years = [1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008]

#data/aviation/airline_ontime/1988/On_Time_On_Time_Performance_1988_10.zip

#for name in glob.glob('/home/marcelo/workspace/2008/*.zip'):
#for name in glob.glob('/home/marcelo/workspace/2008/On_Time_On_Time_Performance_2008_3.zip'):

# errors: 1993/9

#for year in range(1988, 2008):
for year in range(1988, 2009):
    for month in range(1, 13):
        name = '/home/ubuntu/data/aviation/airline_ontime/{0}/On_Time_On_Time_Performance_{0}_{1}.zip'.format(year, month)

        #for name in glob.glob('/home/ubuntu/data/aviation/airline_ontime/YEAR/On_Time_On_Time_Performance_YEAR_MONTH.zip'):
        base = os.path.basename(name)
        filename = os.path.splitext(base)[0]

        datadirectory = '/home/ubuntu/data/aviation/airline_ontime/{0}/'.format(year)
        dataFile = filename
        archive = '.'.join([dataFile, 'zip'])
        fullpath = ''.join([datadirectory, archive])
        csv_file = '.'.join([dataFile, 'csv']) #all fixed

        print csv_file

        try:
            filehandle = open(fullpath, 'rb')
        except Exception as inst:
            print inst.args
            continue

        try:
            zfile = zipfile.ZipFile(filehandle)
        except Exception as inst:
            print inst.args
            continue

        try:
            data = StringIO.StringIO(zfile.read(csv_file)) #don't forget this line!
        except Exception as inst:
            print inst.args
            continue

        reader = csv.DictReader(data)

        fieldnames = [
            'Year',
            'Month',
            'DayofMonth',
            'FlightNum',
            'UniqueCarrier',
            'Origin',
            'Dest',
            'CRSDepTime',
            'DepDel15',
            'ArrDelay',
            'ArrDel15'
        ]
#        clean_file = open('/home/marcelo/workspace/clean-2008/On_Time_On_Time_Performance_2008_3.csv','w+')
        clean_file_name = '/home/ubuntu/clean-data/On_Time_On_Time_Performance_{0}_{1}.csv'.format(year, month)
        clean_file = open(clean_file_name, 'w+')
        writer = csv.DictWriter(clean_file, fieldnames=fieldnames)
        i = 0
        for row in reader:
            if i % 100000 == 0:
    	        print i
            i += 1

    #        writer.writeheader()

            if row['ArrDelay'] != '':
                writer.writerow({
                    'Year': row['Year'], #0
                    'Month': row['Month'], #1
                    'DayofMonth': row['DayofMonth'], #2
                    'FlightNum': row['FlightNum'], #3
                    'UniqueCarrier': row['UniqueCarrier'], #4
                    # 5
                    'Origin': row['Origin'], #5
                    'Dest': row['Dest'], #6
                    'CRSDepTime': row['CRSDepTime'], #7
                    'DepDel15': int(float(row['DepDel15'])), #8
                    'ArrDelay': int(float(row['ArrDelay'])), #9
                    'ArrDel15': int(float(row['ArrDel15'])) #10
                })

print "success! all files were cleaned"
