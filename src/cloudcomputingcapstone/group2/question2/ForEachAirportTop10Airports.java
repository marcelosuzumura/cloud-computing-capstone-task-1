package cloudcomputingcapstone.group2.question2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import cloudcomputingcapstone.Pair;
import cloudcomputingcapstone.TextArrayWritable;

public class ForEachAirportTop10Airports {

	public static class ForEachAirportTop10AirportsMapper extends Mapper<Object, Text, Text, IntWritable> {

		private static final IntWritable ZERO = new IntWritable(0);
		private static final IntWritable ONE = new IntWritable(1);

		private Text keyOut = new Text();

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] values = value.toString().split(",");

			String origin = values[5];
			String destination = values[6];
			int departureDelay = Integer.valueOf(values[8]);

			this.keyOut.set(origin + "," + destination);
			if (departureDelay == 0) {
				context.write(this.keyOut, ONE);
			} else {
				context.write(this.keyOut, ZERO);
			}
		}
	}

	public static class ForEachAirportTop10AirportsReducer extends Reducer<Text, IntWritable, Text, FloatWritable> {

		private FloatWritable performance = new FloatWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int totalFlights = 0;
			float totalOntime = 0;
			for (IntWritable ontimeIntWritable : values) {
				int ontime = ontimeIntWritable.get();
				totalOntime += ontime;
				totalFlights++;
			}
			this.performance.set(totalOntime / totalFlights);
			context.write(key, this.performance);
		}
	}

	public static class TopMapper extends Mapper<Text, Text, NullWritable, TextArrayWritable> {

		private Map<String, TreeSet<Pair<Float, String>>> originDestinationPerformanceMap = new HashMap<>();

		@Override
		public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
			Float performance = Float.valueOf(value.toString());

			String[] originAndDestination = key.toString().split(",");
			String origin = originAndDestination[0];
			String destination = originAndDestination[1];

			if (this.originDestinationPerformanceMap.get(origin) == null) {
				this.originDestinationPerformanceMap.put(origin, new TreeSet<Pair<Float, String>>());
			}

			TreeSet<Pair<Float, String>> performanceDestinationMap = this.originDestinationPerformanceMap.get(origin);
			performanceDestinationMap.add(new Pair<Float, String>(performance, destination));

			if (performanceDestinationMap.size() > 10) {
				performanceDestinationMap.remove(performanceDestinationMap.first());
			}
		}

		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			for (Entry<String, TreeSet<Pair<Float, String>>> originDestinationPerformanceMapEntry : this.originDestinationPerformanceMap
					.entrySet()) {
				for (Pair<Float, String> item : originDestinationPerformanceMapEntry.getValue()) {
					String[] strings = { originDestinationPerformanceMapEntry.getKey(), item.second,
							item.first.toString() };
					TextArrayWritable val = new TextArrayWritable(strings);
					context.write(NullWritable.get(), val);
				}
			}
		}
	}

	public static class TopReducer extends Reducer<NullWritable, TextArrayWritable, Text, FloatWritable> {

		@Override
		public void reduce(NullWritable key, Iterable<TextArrayWritable> values, Context context)
				throws IOException, InterruptedException {
			for (TextArrayWritable val : values) {
				Text[] originDestinationPerformance = (Text[]) val.toArray();
				String origin = originDestinationPerformance[0].toString();
				String destination = originDestinationPerformance[1].toString();
				Float performance = Float.valueOf(originDestinationPerformance[2].toString());
				context.write(new Text(origin + "," + destination), new FloatWritable(performance));
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path tmpPath = new Path("tmp");
		fs.delete(tmpPath, true);

		Path outputPath = new Path(args[1]);
		fs.delete(outputPath, true);

		Job jobA = Job.getInstance(conf, "for each airport top 10 carriers");

		jobA.setMapperClass(ForEachAirportTop10AirportsMapper.class);
		jobA.setReducerClass(ForEachAirportTop10AirportsReducer.class);

		jobA.setOutputKeyClass(Text.class);
		jobA.setOutputValueClass(IntWritable.class);

		FileInputFormat.setInputPaths(jobA, new Path(args[0]));
		FileOutputFormat.setOutputPath(jobA, tmpPath);

		jobA.setJarByClass(ForEachAirportTop10Airports.class);
		jobA.waitForCompletion(true);

		// top 10

		Job jobB = Job.getInstance(conf, "top 10");

		jobB.setMapOutputKeyClass(NullWritable.class);
		jobB.setMapOutputValueClass(TextArrayWritable.class);

		jobB.setOutputKeyClass(Text.class);
		jobB.setOutputValueClass(IntWritable.class);

		jobB.setMapperClass(TopMapper.class);
		jobB.setReducerClass(TopReducer.class);
		jobB.setNumReduceTasks(1);

		FileInputFormat.setInputPaths(jobB, tmpPath);
		FileOutputFormat.setOutputPath(jobB, new Path(args[1]));

		jobB.setInputFormatClass(KeyValueTextInputFormat.class);
		jobB.setOutputFormatClass(TextOutputFormat.class);

		jobB.setJarByClass(ForEachAirportTop10Airports.class);

		System.exit(jobB.waitForCompletion(true) ? 0 : 1);
	}

}
