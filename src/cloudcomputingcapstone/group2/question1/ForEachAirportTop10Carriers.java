package cloudcomputingcapstone.group2.question1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import cloudcomputingcapstone.Pair;
import cloudcomputingcapstone.TextArrayWritable;

public class ForEachAirportTop10Carriers {

	public static class ForEachAirportTop10CarriersMapper extends Mapper<Object, Text, Text, IntWritable> {

		private static final IntWritable ZERO = new IntWritable(0);
		private static final IntWritable ONE = new IntWritable(1);

		private Text keyOut = new Text();

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] values = value.toString().split(",");

			String origin = values[5];
			String carrier = values[4];
			int departureDelay = Integer.valueOf(values[8]);

			this.keyOut.set(origin + "," + carrier);
			if (departureDelay == 0) {
				context.write(this.keyOut, ONE);
			} else {
				context.write(this.keyOut, ZERO);
			}
		}
	}

	public static class ForEachAirportTop10CarriersReducer extends Reducer<Text, IntWritable, Text, FloatWritable> {

		private FloatWritable performance = new FloatWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int totalFlights = 0;
			float totalOntime = 0;
			for (IntWritable ontimeIntWritable : values) {
				int ontime = ontimeIntWritable.get();
				totalOntime += ontime;
				totalFlights++;
			}
			this.performance.set(totalOntime / totalFlights);
			context.write(key, this.performance);
		}
	}

	public static class TopMapper extends Mapper<Text, Text, NullWritable, TextArrayWritable> {

		private Map<String, TreeSet<Pair<Float, String>>> originCarrierPerformanceMap = new HashMap<>();

		@Override
		public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
			Float performance = Float.valueOf(value.toString());

			String[] originAndCarrier = key.toString().split(",");
			String origin = originAndCarrier[0];
			String carrier = originAndCarrier[1];

			if (this.originCarrierPerformanceMap.get(origin) == null) {
				this.originCarrierPerformanceMap.put(origin, new TreeSet<Pair<Float, String>>());
			}

			TreeSet<Pair<Float, String>> performanceCarrierMap = this.originCarrierPerformanceMap.get(origin);
			performanceCarrierMap.add(new Pair<Float, String>(performance, carrier));

			if (performanceCarrierMap.size() > 10) {
				performanceCarrierMap.remove(performanceCarrierMap.first());
			}
		}

		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			for (Entry<String, TreeSet<Pair<Float, String>>> originCarrierPerformanceMapEntry : this.originCarrierPerformanceMap
					.entrySet()) {
				for (Pair<Float, String> item : originCarrierPerformanceMapEntry.getValue()) {
					String[] strings = { originCarrierPerformanceMapEntry.getKey(), item.second,
							item.first.toString() };
					TextArrayWritable val = new TextArrayWritable(strings);
					context.write(NullWritable.get(), val);
				}
			}
		}
	}

	public static class TopReducer extends Reducer<NullWritable, TextArrayWritable, Text, FloatWritable> {

		@Override
		public void reduce(NullWritable key, Iterable<TextArrayWritable> values, Context context)
				throws IOException, InterruptedException {
			for (TextArrayWritable val : values) {
				Text[] originCarrierPerformance = (Text[]) val.toArray();
				String origin = originCarrierPerformance[0].toString();
				String carrier = originCarrierPerformance[1].toString();
				Float performance = Float.valueOf(originCarrierPerformance[2].toString());
				context.write(new Text(origin + "," + carrier), new FloatWritable(performance));
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path tmpPath = new Path("tmp");
		fs.delete(tmpPath, true);

		Path outputPath = new Path(args[1]);
		fs.delete(outputPath, true);

		Job jobA = Job.getInstance(conf, "for each airport top 10 carriers");

		jobA.setMapperClass(ForEachAirportTop10CarriersMapper.class);
		jobA.setReducerClass(ForEachAirportTop10CarriersReducer.class);

		jobA.setOutputKeyClass(Text.class);
		jobA.setOutputValueClass(IntWritable.class);

		FileInputFormat.setInputPaths(jobA, new Path(args[0]));
		FileOutputFormat.setOutputPath(jobA, tmpPath);

		jobA.setJarByClass(ForEachAirportTop10Carriers.class);
		jobA.waitForCompletion(true);

		// top 10

		Job jobB = Job.getInstance(conf, "top 10");

		jobB.setMapOutputKeyClass(NullWritable.class);
		jobB.setMapOutputValueClass(TextArrayWritable.class);

		jobB.setOutputKeyClass(Text.class);
		jobB.setOutputValueClass(IntWritable.class);

		jobB.setMapperClass(TopMapper.class);
		jobB.setReducerClass(TopReducer.class);
		jobB.setNumReduceTasks(1);

		FileInputFormat.setInputPaths(jobB, tmpPath);
		FileOutputFormat.setOutputPath(jobB, new Path(args[1]));

		jobB.setInputFormatClass(KeyValueTextInputFormat.class);
		jobB.setOutputFormatClass(TextOutputFormat.class);

		jobB.setJarByClass(ForEachAirportTop10Carriers.class);

		System.exit(jobB.waitForCompletion(true) ? 0 : 1);
	}

}
