package cloudcomputingcapstone.group2.question4;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class ForEachOriginDestinationMeanArrivalDelay {

	// For each source-destination pair X-Y, determine the mean arrival delay
	// (in minutes) for a flight from X to Y.

	public static class ForEachOriginDestinationMeanArrivalDelayMapper extends Mapper<Object, Text, Text, IntWritable> {

		private Text keyOut = new Text();

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] values = value.toString().split(",");

			String origin = values[5];
			String destination = values[6];
			int arrivalDelay = Integer.valueOf(values[9]);

			this.keyOut.set(origin + "," + destination);
			context.write(this.keyOut, new IntWritable(arrivalDelay));
		}
	}

	public static class ForEachOriginDestinationMeanArrivalDelayReducer
			extends Reducer<Text, IntWritable, Text, FloatWritable> {

		private FloatWritable meanArrivalDelay = new FloatWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int totalFlights = 0;
			float totalArrivalDelay = 0;
			for (IntWritable arrivalDelayWritable : values) {
				int arrivalDelay = arrivalDelayWritable.get();
				totalArrivalDelay += arrivalDelay;
				totalFlights++;
			}
			this.meanArrivalDelay.set(totalArrivalDelay / totalFlights);
			context.write(key, this.meanArrivalDelay);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);

		Path outputPath = new Path(args[1]);
		fs.delete(outputPath, true);

		Job jobA = Job.getInstance(conf, "for each origin destination mean arrival delay");

		jobA.setMapperClass(ForEachOriginDestinationMeanArrivalDelayMapper.class);
		jobA.setReducerClass(ForEachOriginDestinationMeanArrivalDelayReducer.class);

		jobA.setOutputKeyClass(Text.class);
		jobA.setOutputValueClass(IntWritable.class);

		FileInputFormat.setInputPaths(jobA, new Path(args[0]));
		FileOutputFormat.setOutputPath(jobA, new Path(args[1]));

		jobA.setJarByClass(ForEachOriginDestinationMeanArrivalDelay.class);
		jobA.waitForCompletion(true);

		System.exit(jobA.waitForCompletion(true) ? 0 : 1);
	}

}
