package cloudcomputingcapstone.group3.question1;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class AirportPopularity {

	public static class AirportPopularityMapper extends Mapper<Object, Text, Text, IntWritable> {

		private static final IntWritable ONE = new IntWritable(1);

		private Text airport = new Text();

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] values = value.toString().split(",");

			this.airport.set(values[5]);
			context.write(this.airport, ONE);

			this.airport.set(values[6]);
			context.write(this.airport, ONE);
		}
	}

	public static class AirportPopularityReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		private IntWritable result = new IntWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			this.result.set(sum);
			context.write(key, this.result);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);

		Path outputPath = new Path(args[1]);
		fs.delete(outputPath, true);

		// popularity

		Job jobA = Job.getInstance(conf, "airport popularity");
		jobA.setOutputKeyClass(Text.class);
		jobA.setOutputValueClass(IntWritable.class);
		jobA.setMapperClass(AirportPopularityMapper.class);
		jobA.setReducerClass(AirportPopularityReducer.class);

		FileInputFormat.setInputPaths(jobA, new Path(args[0]));
		FileOutputFormat.setOutputPath(jobA, new Path(args[1]));

		jobA.setJarByClass(AirportPopularity.class);

		System.exit(jobA.waitForCompletion(true) ? 0 : 1);
	}

}
