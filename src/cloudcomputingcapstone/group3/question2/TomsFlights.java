package cloudcomputingcapstone.group3.question2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@SuppressWarnings("unused")
public class TomsFlights extends Configured implements Tool {

	public static class TomsFlightsMapper extends MapReduceBase implements Mapper<Object, Text, Text, Text> {

		private Text flightKey = new Text();

		@Override
		public void map(Object key, Text value, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			FlightCSVFormat flightCSVFormat = FlightCSVFormat.parseCSVLine(value);

			this.flightKey.set(flightCSVFormat.getFlightKey());
			output.collect(this.flightKey, value);
		}
	}

	static class Flights {
		private FlightCSVFormat firstLegFlight;
		private FlightCSVFormat secondLegFlight;

		public Flights(FlightCSVFormat firstLegFlight, FlightCSVFormat secondLegFlight) {
			this.firstLegFlight = firstLegFlight;
			this.secondLegFlight = secondLegFlight;
		}
	}

	public static class TomsFlightsReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

		private Text value = new Text();

		@Override
		public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			FlightCSVFormat firstLegFlightWithLessDelay = null;
			FlightCSVFormat secondLegFlightWithLessDelay = null;

			List<FlightCSVFormat> firstLegFlights = Lists.newArrayList();
			List<FlightCSVFormat> secondLegFlights = Lists.newArrayList();

			int i = 0;
			while (values.hasNext()) {
				FlightCSVFormat flightCSVFormat = FlightCSVFormat.parseCSVLine(values.next());
				if (flightCSVFormat.canBeFirstLegFlight()) {
					firstLegFlights.add(flightCSVFormat);
				} else { // 2nd leg flights
					secondLegFlights.add(flightCSVFormat);
				}

				i++;
			}

			// matches first leg flights with second leg flights
			Map<String, Flights> minimumDelayFlights = Maps.newHashMap();
			for (FlightCSVFormat firstLegFlight : firstLegFlights) {
				for (FlightCSVFormat secondLegFlight : secondLegFlights) {
					String itinerary = firstLegFlight.getOrigin() + "-" + firstLegFlight.getDestination() + "-"
							+ secondLegFlight.getDestination();

					Flights flights = minimumDelayFlights.get(itinerary);
					if (flights == null) {
						flights = new Flights(firstLegFlight, secondLegFlight);
						minimumDelayFlights.put(itinerary, flights);
					}

					if (firstLegFlight.getArrivalDelay() < flights.firstLegFlight.getArrivalDelay()) {
						flights.firstLegFlight = firstLegFlight;
					}

					if (secondLegFlight.getArrivalDelay() < flights.secondLegFlight.getArrivalDelay()) {
						flights.secondLegFlight = secondLegFlight;
					}
				}
			}

			// output matches
			for (Flights flights : minimumDelayFlights.values()) {
				this.value.set(flights.firstLegFlight.getOrigin() + "," //
						+ flights.firstLegFlight.getDestination() + "," //
						+ flights.secondLegFlight.getDestination() + "," //
						+ flights.firstLegFlight.getDay() + "," //
						+ flights.firstLegFlight.getMonth() + "," //
						+ flights.firstLegFlight.getFlightNum() + "," //
						+ flights.secondLegFlight.getFlightNum());
				output.collect(key, this.value);
			}
		}
	}

	static int printUsage() {
		System.out.println("wordcount [-m <maps>] [-r <reduces>] <input> <output>");
		ToolRunner.printGenericCommandUsage(System.out);
		return -1;
	}

	/**
	 * The main driver for word count map/reduce program. Invoke this method to submit the map/reduce job.
	 *
	 * @throws IOException
	 *             When there is communication problems with the job tracker.
	 */
	@Override
	public int run(String[] args) throws Exception {
		JobConf conf = new JobConf(this.getConf(), TomsFlights.class);
		conf.setJobName("toms_flights");

		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		conf.setMapperClass(TomsFlightsMapper.class);
		// conf.setCombinerClass(TomsFlightsReducer.class);
		conf.setReducerClass(TomsFlightsReducer.class);

		List<String> other_args = new ArrayList<String>();
		for (int i = 0; i < args.length; ++i) {
			try {
				if ("-m".equals(args[i])) {
					conf.setNumMapTasks(Integer.parseInt(args[++i]));
				} else if ("-r".equals(args[i])) {
					conf.setNumReduceTasks(Integer.parseInt(args[++i]));
				} else {
					other_args.add(args[i]);
				}
			} catch (NumberFormatException except) {
				System.out.println("ERROR: Integer expected instead of " + args[i]);
				return printUsage();
			} catch (ArrayIndexOutOfBoundsException except) {
				System.out.println("ERROR: Required parameter missing from " + args[i - 1]);
				return printUsage();
			}
		}
		// Make sure there are exactly 2 parameters left.
		if (other_args.size() != 2) {
			System.out.println("ERROR: Wrong number of parameters: " + other_args.size() + " instead of 2.");
			return printUsage();
		}

		FileSystem fs = FileSystem.get(this.getConf());
		Path outputPath = new Path(other_args.get(1));
		fs.delete(outputPath, true);

		FileInputFormat.setInputPaths(conf, other_args.get(0));
		FileOutputFormat.setOutputPath(conf, new Path(other_args.get(1)));

		JobClient.runJob(conf);
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new TomsFlights(), args);
		System.exit(res);
	}

}
