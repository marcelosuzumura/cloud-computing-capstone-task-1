package cloudcomputingcapstone.group3.question2;

import java.util.Calendar;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import cloudcomputingcapstone.group3.question2.TomsFlights.TomsFlightsMapper;

public class FlightCSVFormat {

	private static final Logger logger = Logger.getLogger(TomsFlightsMapper.class);

	private String year;
	private String month;
	private String day;
	private String flightNum;
	private String origin;
	private String destination;
	private String departureTime;
	private int arrivalDelay;

	private FlightCSVFormat(String year, String month, String day, String flightNum, String origin, String destination,
			String departureTime, int arrivalDelay) {
		super();
		this.year = year;
		this.month = month;
		this.day = day;
		this.flightNum = flightNum;
		this.origin = origin;
		this.destination = destination;
		this.departureTime = departureTime;
		this.arrivalDelay = arrivalDelay;
	}

	public static FlightCSVFormat parseCSVLine(Text value) {
		String[] values = value.toString().split(",");

		try {
			String year = values[0];
			String month = values[1];
			String day = values[2];
			String flightNum = values[3];
			String origin = values[5];
			String destination = values[6];
			String departureTime = values[7];
			int arrivalDelay = Integer.valueOf(values[9]);

			return new FlightCSVFormat(year, month, day, flightNum, origin, destination, departureTime, arrivalDelay);
		} catch (ArrayIndexOutOfBoundsException e) {
			logger.error("array out of bounds exception: " + value.toString());
			throw e;
		}
	}

	public int getDepartureTimeHour() {
		// get the hour from time: e.g. 903 / 100 = 9; 2050 / 100 = 20
		int departureHour = Integer.valueOf(this.departureTime) / 100;
		return departureHour;
	}

	public int getDepartureTimeMinutes() {
		// get the minutes from time: e.g. 903 % 100 = 3; 2050 % 100 = 50
		int departureMinutes = Integer.valueOf(this.departureTime) % 100;
		return departureMinutes;
	}

	public String getFlightKey() {
		if (this.canBeFirstLegFlight()) {
			return this.destination + "-" + this.day + "/" + this.month + "/" + this.year;
		} else {
			// adjust date to coincide with first leg dates, so it will be
			// grouped together on reduce phase
			// e.g. 29/1 will be adjusted to 27/1
			// e.g. 1/2 will be adjusted to 30/1
			Calendar canBeSecondLegFlightDateMinus2Days = Calendar.getInstance();
			canBeSecondLegFlightDateMinus2Days.set(Integer.valueOf(this.year), Integer.valueOf(this.month) - 1,
					Integer.valueOf(this.day));
			canBeSecondLegFlightDateMinus2Days.add(Calendar.DAY_OF_MONTH, -2);
			int dayAdjusted = canBeSecondLegFlightDateMinus2Days.get(Calendar.DAY_OF_MONTH);
			int monthAdjusted = canBeSecondLegFlightDateMinus2Days.get(Calendar.MONTH) + 1;

			return this.origin + "-" + dayAdjusted + "/" + monthAdjusted + "/"
					+ canBeSecondLegFlightDateMinus2Days.get(Calendar.YEAR);
		}
	}

	public boolean canBeFirstLegFlight() {
		boolean canBeFirstLegFlight;
		if (this.getDepartureTimeHour() < 12
				|| (this.getDepartureTimeHour() == 12 && this.getDepartureTimeMinutes() == 0)) {
			canBeFirstLegFlight = true;
		} else {
			canBeFirstLegFlight = false;
		}
		return canBeFirstLegFlight;
	}

	// public String getFlightValue() {
	// StringBuilder value = new StringBuilder();
	// value.append(this.flightNum);
	// value.append(",");
	// value.append(this.origin);
	// value.append(",");
	// value.append(this.destination);
	// value.append(",");
	// value.append(this.year);
	// value.append(",");
	// value.append(this.month);
	// value.append(",");
	// value.append(this.day);
	// value.append(",");
	// value.append(this.departureTime);
	// value.append(",");
	// value.append(this.arrivalDelay);
	// value.append(",");
	// value.append(this.canBeFirstLegFlight());
	// value.append(",");
	// return value.toString();
	// }

	// public static int getArrivalDelay(Text value) {
	// String[] values = value.toString().split(",");
	// // String flightNum = values[0];
	// // String origin = values[1];
	// // String destination = values[2];
	// // String year = values[3];
	// // String month = values[4];
	// // String day = values[5];
	// // String departureTimeHour = values[6];
	// // String departureTimeMinutes = values[7];
	// int arrivalDelay = Integer.valueOf(values[7]);
	// // boolean canBeFirstLegFlight = Boolean.valueOf(values[8]);
	//
	// return arrivalDelay;
	// }

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return this.day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getFlightNum() {
		return this.flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public String getOrigin() {
		return this.origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureTime() {
		return this.departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public int getArrivalDelay() {
		return this.arrivalDelay;
	}

	public void setArrivalDelay(int arrivalDelay) {
		this.arrivalDelay = arrivalDelay;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

}
