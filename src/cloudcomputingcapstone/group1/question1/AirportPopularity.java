package cloudcomputingcapstone.group1.question1;

import java.io.IOException;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import cloudcomputingcapstone.Pair;
import cloudcomputingcapstone.TextArrayWritable;

public class AirportPopularity {

	public static class AirportPopularityMapper extends Mapper<Object, Text, Text, IntWritable> {

		private static final IntWritable ONE = new IntWritable(1);

		private Text airport = new Text();

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] values = value.toString().split(",");

			this.airport.set(values[5]);
			context.write(this.airport, ONE);

			this.airport.set(values[6]);
			context.write(this.airport, ONE);
		}
	}

	public static class AirportPopularityReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		private IntWritable result = new IntWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			this.result.set(sum);
			context.write(key, this.result);
		}
	}

	public static class TopAirportPopularityMapper extends Mapper<Text, Text, NullWritable, TextArrayWritable> {

		private TreeSet<Pair<Integer, String>> countToAirportMap = new TreeSet<Pair<Integer, String>>();

		@Override
		public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
			Integer count = Integer.parseInt(value.toString());
			String airport = key.toString();
			this.countToAirportMap.add(new Pair<Integer, String>(count, airport));

			if (this.countToAirportMap.size() > 10) {
				this.countToAirportMap.remove(this.countToAirportMap.first());
			}
		}

		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			for (Pair<Integer, String> item : this.countToAirportMap) {
				String[] strings = { item.second, item.first.toString() };
				TextArrayWritable val = new TextArrayWritable(strings);
				context.write(NullWritable.get(), val);
			}
		}
	}

	public static class TopAirportPopularityReducer
			extends Reducer<NullWritable, TextArrayWritable, Text, IntWritable> {

		private TreeSet<Pair<Integer, String>> countToAirportMap = new TreeSet<Pair<Integer, String>>();

		@Override
		public void reduce(NullWritable key, Iterable<TextArrayWritable> values, Context context)
				throws IOException, InterruptedException {
			for (TextArrayWritable val : values) {
				Text[] pair = (Text[]) val.toArray();
				String airport = pair[0].toString();
				Integer count = Integer.parseInt(pair[1].toString());
				this.countToAirportMap.add(new Pair<Integer, String>(count, airport));
				if (this.countToAirportMap.size() > 10) {
					this.countToAirportMap.remove(this.countToAirportMap.first());
				}
			}
			for (Pair<Integer, String> item : this.countToAirportMap) {
				Text word = new Text(item.second);
				IntWritable value = new IntWritable(item.first);
				context.write(word, value);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path tmpPath = new Path("tmp");
		fs.delete(tmpPath, true);

		Path outputPath = new Path(args[1]);
		fs.delete(outputPath, true);

		// popularity

		Job jobA = Job.getInstance(conf, "airport popularity");
		jobA.setOutputKeyClass(Text.class);
		jobA.setOutputValueClass(IntWritable.class);
		jobA.setMapperClass(AirportPopularityMapper.class);
		jobA.setReducerClass(AirportPopularityReducer.class);

		FileInputFormat.setInputPaths(jobA, new Path(args[0]));
		FileOutputFormat.setOutputPath(jobA, tmpPath);

		jobA.setJarByClass(AirportPopularity.class);
		jobA.waitForCompletion(true);

		// top 10

		Job jobB = Job.getInstance(conf, "top 10 airports");
		jobB.setMapOutputKeyClass(NullWritable.class);
		jobB.setMapOutputValueClass(TextArrayWritable.class);

		jobB.setOutputKeyClass(Text.class);
		jobB.setOutputValueClass(IntWritable.class);

		jobB.setMapperClass(TopAirportPopularityMapper.class);
		jobB.setReducerClass(TopAirportPopularityReducer.class);
		jobB.setNumReduceTasks(1);

		FileInputFormat.setInputPaths(jobB, tmpPath);
		FileOutputFormat.setOutputPath(jobB, new Path(args[1]));

		jobB.setInputFormatClass(KeyValueTextInputFormat.class);
		jobB.setOutputFormatClass(TextOutputFormat.class);
		jobB.setJarByClass(AirportPopularity.class);

		System.exit(jobB.waitForCompletion(true) ? 0 : 1);
	}

}
