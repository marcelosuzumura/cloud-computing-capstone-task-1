package cloudcomputingcapstone.group1.question2;

import java.io.IOException;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import cloudcomputingcapstone.Pair;
import cloudcomputingcapstone.TextArrayWritable;

public class AirlinePerformance {

	// Rank the top 10 airlines by on-time arrival performance.

	public static class AirlinePerformanceMapper extends Mapper<Object, Text, Text, IntWritable> {

		private static final IntWritable ZERO = new IntWritable(0);
		private static final IntWritable ONE = new IntWritable(1);

		private Text keyOut = new Text();

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] values = value.toString().split(",");

			String carrier = values[4];
			int arrivalDelay = Integer.valueOf(values[10]);

			this.keyOut.set(carrier);
			if (arrivalDelay == 0) {
				context.write(this.keyOut, ONE);
			} else {
				context.write(this.keyOut, ZERO);
			}
		}
	}

	public static class AirlinePerformanceReducer extends Reducer<Text, IntWritable, Text, FloatWritable> {

		private FloatWritable performance = new FloatWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int totalFlights = 0;
			float totalOntime = 0;
			for (IntWritable ontimeIntWritable : values) {
				int ontime = ontimeIntWritable.get();
				totalOntime += ontime;
				totalFlights++;
			}
			this.performance.set(totalOntime / totalFlights);
			context.write(key, this.performance);
		}
	}

	public static class TopAirlinePerformanceMapper extends Mapper<Text, Text, NullWritable, TextArrayWritable> {

		private TreeSet<Pair<Float, String>> performanceToCarrierMap = new TreeSet<Pair<Float, String>>();

		@Override
		public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
			String carrier = key.toString();
			Float performance = Float.valueOf(value.toString());
			this.performanceToCarrierMap.add(new Pair<Float, String>(performance, carrier));

			if (this.performanceToCarrierMap.size() > 10) {
				this.performanceToCarrierMap.remove(this.performanceToCarrierMap.first());
			}
		}

		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			for (Pair<Float, String> item : this.performanceToCarrierMap) {
				String[] strings = { item.second, item.first.toString() };
				TextArrayWritable val = new TextArrayWritable(strings);
				context.write(NullWritable.get(), val);
			}
		}
	}

	public static class TopAirlinePerformanceReducer
			extends Reducer<NullWritable, TextArrayWritable, Text, FloatWritable> {

		private TreeSet<Pair<Float, String>> performanceToCarrierMap = new TreeSet<Pair<Float, String>>();

		@Override
		public void reduce(NullWritable key, Iterable<TextArrayWritable> values, Context context)
				throws IOException, InterruptedException {
			for (TextArrayWritable val : values) {
				Text[] pair = (Text[]) val.toArray();
				String carrier = pair[0].toString();
				Float performance = Float.valueOf(pair[1].toString());

				this.performanceToCarrierMap.add(new Pair<Float, String>(performance, carrier));
				if (this.performanceToCarrierMap.size() > 10) {
					this.performanceToCarrierMap.remove(this.performanceToCarrierMap.first());
				}
			}

			for (Pair<Float, String> item : this.performanceToCarrierMap) {
				Text word = new Text(item.second);
				FloatWritable value = new FloatWritable(item.first);
				context.write(word, value);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path tmpPath = new Path("tmp");
		fs.delete(tmpPath, true);

		Path outputPath = new Path(args[1]);
		fs.delete(outputPath, true);

		// popularity

		Job jobA = Job.getInstance(conf, "airline performance");

		jobA.setMapperClass(AirlinePerformanceMapper.class);
		jobA.setReducerClass(AirlinePerformanceReducer.class);

		jobA.setOutputKeyClass(Text.class);
		jobA.setOutputValueClass(IntWritable.class);

		FileInputFormat.setInputPaths(jobA, new Path(args[0]));
		FileOutputFormat.setOutputPath(jobA, tmpPath);

		jobA.setJarByClass(AirlinePerformance.class);
		jobA.waitForCompletion(true);

		// top 10

		Job jobB = Job.getInstance(conf, "top 10 airlines");
		jobB.setMapOutputKeyClass(NullWritable.class);
		jobB.setMapOutputValueClass(TextArrayWritable.class);

		jobB.setOutputKeyClass(Text.class);
		jobB.setOutputValueClass(FloatWritable.class);

		jobB.setMapperClass(TopAirlinePerformanceMapper.class);
		jobB.setReducerClass(TopAirlinePerformanceReducer.class);
		jobB.setNumReduceTasks(1);

		FileInputFormat.setInputPaths(jobB, tmpPath);
		FileOutputFormat.setOutputPath(jobB, new Path(args[1]));

		jobB.setInputFormatClass(KeyValueTextInputFormat.class);
		jobB.setOutputFormatClass(TextOutputFormat.class);
		jobB.setJarByClass(AirlinePerformance.class);

		System.exit(jobB.waitForCompletion(true) ? 0 : 1);
	}

}
